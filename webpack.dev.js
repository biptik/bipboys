const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: './dist',
    port: 3000,
    historyApiFallback: true,
    hot: true,
  },
  entry: './src/client.tsx',
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.mjs', '.scss', '.sass'],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  output: {
    filename: 'cerber.js',
    publicPath: '/',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
              experimentalWatchApi: true,
            },
          },
        ],
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'postcss-loader' },
          {
            loader: 'sass-loader',
            options: {
              importer(url) {
                if (/^~.+/.test(url)) {
                  const filePath = url.match(/^~(.+)/)[1];
                  const nodeModulePath = `./node_modules/${filePath}`;
                  return {
                    file: path.resolve(nodeModulePath),
                  };
                }
                return {
                  file: url,
                };
              },
            },
          },
        ],
      },
    ],
  },
});
